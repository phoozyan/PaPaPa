﻿using PaPaPa.Core.Caches;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaPaPa.Web.Business.Caches
{
    public class CacheBusiness
    {
        public static void Init()
        {
            RedisCacheCore.Init();
        }
    }
}
