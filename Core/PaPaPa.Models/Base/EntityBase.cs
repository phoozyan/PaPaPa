﻿using Framework.Mapping.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaPaPa.Models.Base
{
    public class EntityBase
    {
        [Required]
        public bool Enabled { get; set; }

        [Required]
        public int CreateUserId { get; set; }

        [Required]
        public int UpdateUserId { get; set; }

        [MapperIgnore]
        public DateTime CreateTime { get; set; }

        [MapperAutoUpdateDateTime(MapperDateTimeType.Now)]
        public DateTime ModifyTime { get; set; }

        public EntityBase()
        {
            CreateTime = DateTime.Now;

            Enabled = true;
        }
    }
}
