﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;

namespace Framework.Redis.Tests
{
    [TestClass]
    public class RedisTest
    {
        [TestMethod]
        public void TestGet()
        {
            var client = RedisSingleton.GetInstance.Client = new Client.RedisClient();
            client.Connect("127.0.0.1", 6379);

            string key = "test";
            string value = "Framework.Redis";

            client.Set(key, value);

            Assert.AreEqual(value, client.Get(key));
        }
    }
}
